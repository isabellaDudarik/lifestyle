package com.example.lifestyle;

import android.app.Service;
import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.IBinder;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class MyService extends Service implements SensorEventListener {
    SensorManager sensorManager;

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }


    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        //timer with rx

        SensorManager sensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
        sensorManager.registerListener(this, sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER),
                SensorManager.SENSOR_DELAY_FASTEST);
        return START_NOT_STICKY;
    }

    List<Coordinates> list = new ArrayList<>();

    class Coordinates {
        float ax;
        float ay;
        float az;

        public Coordinates(float ax, float ay, float az) {
            this.ax = ax;
            this.ay = ay;
            this.az = az;
        }
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        if (event.sensor.getType() == Sensor.TYPE_ACCELEROMETER) {
            list.add(new Coordinates(event.values[0], event.values[1], event.values[2]));
//            Log.d("sdf", event.values[0] + "/" + event.values[1] + "/" + event.values[2]);
            if (list.size() > 50) {
                sensorManager.unregisterListener(this);

                //calc
                List<Float> acceleration = new ArrayList<>();
                float max = acceleration.get(0);
                float min = acceleration.get(0);
                for (int i = 0; i < list.size(); i++) {
                    acceleration.set(i, (float) Math.sqrt(event.values[0] * event.values[0]
                            + event.values[1] * event.values[1]
                            + event.values[2] * event.values[2]));
                    if (acceleration.get(i) > max) {
                        max = acceleration.get(i);
                    }
                    if (acceleration.get(i) < min) {
                        min = acceleration.get(i);
                    }
                }
                float result = max - min;


                //to bd

                MyDataBase myDataBase = MyDataBase.getDataBase(this);
                myDataBase.getResultDao().insertAll(result);
            }
        }

    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }
}
