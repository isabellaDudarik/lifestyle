package com.example.lifestyle;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

@Database(entities = {ResultOfActivity.class}, version = 1)
public abstract class MyDataBase extends RoomDatabase {
    public abstract MyDataBaseDao getResultDao();

    private static MyDataBase INSTANCE;

    public static MyDataBase getDataBase(Context context) {
        if (INSTANCE == null) {
            INSTANCE =
                    Room.databaseBuilder(context.getApplicationContext(), MyDataBase.class, "database")
                            .allowMainThreadQueries()
                            .fallbackToDestructiveMigration()
                            .build();
            return INSTANCE;
        } else return INSTANCE;
    }
    public static void DestroyDatabase(){
        INSTANCE = null;
    }

}