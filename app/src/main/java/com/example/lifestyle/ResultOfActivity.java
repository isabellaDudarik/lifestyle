package com.example.lifestyle;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity
public class ResultOfActivity {
    @PrimaryKey
    public int id;
    public float result;

    public ResultOfActivity() {
    }

    public ResultOfActivity(int id, float result) {
        this.id = id;
        this.result = result;
    }
}
