package com.example.lifestyle;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.List;

@Dao
public abstract class MyDataBaseDao {
    @Insert
    public abstract void insertAll(List<ResultOfActivity> resultOfActivities);

    @Query("SELECT * FROM ResultOfActivity")
    public abstract List<ResultOfActivity> selectAll();

    @Query("DELETE FROM ResultOfActivity")
    public abstract void removeAll();
}
